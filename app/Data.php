<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $table    = 'data';
    protected $fillable = ['device_id', 'waktu', 'waktu_akurat', 'sensor1', 'sensor2', 'sensor3'];
    protected $primaryKey = 'id';
}
