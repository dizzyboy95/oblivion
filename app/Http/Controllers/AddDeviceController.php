<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Control;
use App\Device;
use App\User;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Schema;

class AddDeviceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('main.adddevice');
    }

    protected function add(request $request)
    {
        $data = $request->all();
      if (Device::where ('device_id','=',Input::get('input_id'))->first() &&
          Device::where ('username','=',Input::get('username'))->first())
        {
            Session::flash('failed', ' Device ID dengan Username yang sama sudah pernah diinputkan!');
        } 
        else 
        {                               
            // input data into database
            Device::create ([
                'username'      => $data ['username'],
                'device_id'     => $data ['input_id'],
                'generated_id'   => $data ['device_id']
                // 'device_job'    => $data ['device_job']
            ]);

            Control::create ([
                'username'      => $data['username'],
                'device_id'     => $data ['input_id']                
            ]);

            Session::flash('success', ' Device sukses didaftarkan !');            
        }

        return redirect()->route('home');
    }
}