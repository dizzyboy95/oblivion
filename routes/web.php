<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/update', 'HomeController@update')->name('udpate');
Route::post('/adddevice', 'AddDeviceController@index')->name('adddevice');
Route::get('/adddevice', 'AddDeviceController@add')->name('submitdevice');

//new-update
Route::post('/ctl', 'ControlController@control')->name('controldevice');
Route::post('/add', 'AddDataController@add')->name('adddata');
